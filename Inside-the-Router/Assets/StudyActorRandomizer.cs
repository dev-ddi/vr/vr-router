using OmiLAXR.xAPI.Tracking;
using UnityEngine;

public class StudyActorRandomizer : MonoBehaviour
{
    public LrsController LRSController;

    private const string CodeKey = "HMD-Code";

    private void Awake()
    {
        LRSController.userName = PlayerPrefs.GetString(CodeKey);
    }
}
