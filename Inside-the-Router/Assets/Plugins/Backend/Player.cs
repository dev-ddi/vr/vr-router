﻿using System;

public class Player
{
    public String Name { get; set; }
    public UInt32 Score { get; set; }
}
