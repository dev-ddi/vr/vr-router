﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class IpUtilsTest
    {
        /*
         *      IP                           BIN                      DEC
         * 141.030.032.072 - 10001101.00011110.00100000.01001000 - 2.367.561.800
         * 192.168.000.000 - 11000000.10101000.00000000.00000000 - 3.232.235.520
         * 192.168.001.000 - 11000000.10101000.00000001.00000000 - 3.232.235.776
         */

        [Test]
        public void ConvertUIntToString()
        {
            UInt32 external = 0b_1000_1101_0001_1110_0010_0000_0100_1000;
            UInt32 internal0 = 0b_1100_0000_1010_1000_0000_0000_0000_0000;
            UInt32 internal1 = 0b_1100_0000_1010_1000_0000_0001_0000_0000;

            String str_external = IpUtils.ConvertUIntToString(external);
            String str_internal0 = IpUtils.ConvertUIntToString(internal0);
            String str_internal1 = IpUtils.ConvertUIntToString(internal1);

            Assert.AreEqual("141.30.32.72", str_external, "Conversion of External IP incorrect");
            Assert.AreEqual("192.168.0.0", str_internal0, "Conversion of Internal IP incorrect");
            Assert.AreEqual("192.168.1.0", str_internal1, "Conversion of Internal IP incorrect");
        }

        [Test]
        public void ConvertAddressToString()
        {
            Address external = new Address()
            {
                IpAddress = 0b_1000_1101_0001_1110_0010_0000_0100_1000,
                Port = 81
            };

            Address internal0 = new Address()
            {
                IpAddress = 0b_1100_0000_1010_1000_0000_0000_0000_0000,
                Port = 6001
            };

            Address internal1 = new Address()
            {
                IpAddress = 0b_1100_0000_1010_1000_0000_0001_0000_0000,
                Port = 8081
            };

            String str_external = IpUtils.ConvertAddressToString(external);
            String str_internal0 = IpUtils.ConvertAddressToString(internal0);
            String str_internal1 = IpUtils.ConvertAddressToString(internal1);

            Assert.AreEqual("141.30.32.72:81", str_external, "Conversion of External IP incorrect");
            Assert.AreEqual("192.168.0.0:6001", str_internal0, "Conversion of Internal IP incorrect");
            Assert.AreEqual("192.168.1.0:8081", str_internal1, "Conversion of Internal IP incorrect");
        }
    }
}
