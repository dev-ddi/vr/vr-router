﻿using System.Collections;
using System.Collections.Generic;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

/// <summary>
/// This script controls the lifecycle of a tube and its reactions to arriving packages that were thrown at it
/// </summary>
public class TubeScript : FeedbackChannel
{
    public Text TubeLabel;

    public AudioSource CorrectSound;
    public AudioSource WrongSound;

    private Tube tube;

    public MeshRenderer RimRenderer;
    public Material RedMaterial;
    public Material GreenMaterial;
    public Light AttentionSpotLight;

    Material[] redMaterialSet = new Material[2];
    Material[] greenMaterialSet = new Material[2];
    Material[] defaultMaterialSet;

    public List<Collider> MutedColliders = new();

    private Color accentColor = new Color();
    private Color baseColor = new Color();
    private bool isFeedbackActive = false;


    private void Awake()
    {
        redMaterialSet[0] = RimRenderer.materials[0];
        redMaterialSet[1] = RedMaterial;
        greenMaterialSet[0] = RimRenderer.materials[0];
        greenMaterialSet[1] = GreenMaterial;
        defaultMaterialSet = RimRenderer.materials;
        baseColor = TubeLabel.color;
        accentColor = GreenMaterial.color;
    }

    public void FlashForSeconds(float t)
    {
        if(AttentionSpotLight != null)
        {
            StartCoroutine(Flash(t));
        }
    }

    IEnumerator Flash(float t)
    {
        AttentionSpotLight.intensity = 100;
        yield return new WaitForSeconds(t);
        AttentionSpotLight.intensity = 0;

    }

    public void SetTube(Tube t)
    {
        if (tube != null)
        {
            tube.CorrectPackageReceived -= CorrectPackageReceived;
            tube.WrongPackageReceived -= WrongPackageReceived;
        }

        tube = t;

        if (TubeLabel == null || t == null)
            return;

        tube.CorrectPackageReceived += CorrectPackageReceived;
        tube.WrongPackageReceived += WrongPackageReceived;

        var txt = IpUtils.ConvertUIntToString(t.IpAddress);
        TubeLabel.text = txt;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (MutedColliders.Contains(other))
            return;

        if (other.gameObject == null)
            return;

        var packageScript = other.GetComponent<PackageScript>();

        if (packageScript == null)
            return;

        var package = packageScript.GetDataPackage();


        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "GameNoPedestals")
        {
            packageScript.DisapperInTube(gameObject);
            StartCoroutine(WaitForPackageToDisappear(package, packageScript));
        }
        else
        {
            if (package != null)
                tube.HandlePackage(package);

            RunController.CurrentRunController.DestroyPackage(packageScript);
        }
    }

    private IEnumerator WaitForPackageToDisappear(Package package, PackageScript packageScript)
    {
        yield return new WaitForSeconds(0.6f);
        if (package != null)
            tube.HandlePackage(package);

        RunController.CurrentRunController.DestroyPackage(packageScript);
    }

    private void WrongPackageReceived(object sender, RunEventArgs e)
    {
        StartCoroutine(ToggleLightRoutine(1f, false));
        if (WrongSound != null)
        {
            WrongSound.Play();
        }
    }

    private void CorrectPackageReceived(object sender, RunEventArgs e)
    {
        isFeedbackActive = false;
        StartCoroutine(ToggleLightRoutine(1f, true));
        if (CorrectSound != null) CorrectSound.Play();
    }

    /// <summary>
    /// This routine switches the tubes rim material to green or red for a given amount of time.
    /// </summary>
    private IEnumerator ToggleLightRoutine(float time, bool isGreenLight)
    {
        RimRenderer.materials = isGreenLight ? greenMaterialSet : redMaterialSet;
        yield return new WaitForSeconds(time);
        RimRenderer.materials = defaultMaterialSet;
    }


    public void MuteCollider(Collider col, float seconds)
    {
        MutedColliders.Add(col);
        StartCoroutine(AllowCollider(col, seconds));
    }

    private IEnumerator AllowCollider(Collider col, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        MutedColliders.Remove(col);
    }

    [ContextMenu("Activate Feedback")]
    public void activateFeedback()
    {
        OnFeedbackRequested(FeedbackCase.UNKNOWN);
    }

    public override void OnFeedbackRequested(FeedbackCase fbc)
    {
        if (!IsFeedbackRelevantForThisGameobject()) return;
        StartCoroutine(HighlightLabel(TubeLabel));
    }

    private bool IsFeedbackRelevantForThisGameobject()
    {
        var p = RunController.CurrentRunController.CurrentPackages[0];
        var packageScript = p.GetComponent<PackageScript>();
        var dataPackage = packageScript.GetDataPackage();

        //Paket ist direkt an Röhre addressiert
        if (dataPackage.Destination.IpAddress == tube.IpAddress) return true;

        //Paket nutzt NAT
        foreach(var entry in NatTable.Entries)
        {
            //Relevanter NAT Eintrag existiert
            if (
                entry.InternalAddress.IpAddress == tube.IpAddress &&
                entry.ExternalAddress == dataPackage.Destination)
                return true;
        }

        //Paket ist ausgehend
        if (tube.IpAddress == IpUtils.IspIP && !dataPackage.Destination.StrAddress.Contains("192.168"))
        {
            return true;
        }

        return false;
    }

    public IEnumerator HighlightLabel(Text feedBackText)
    {
        Debug.Log("Enum_Fade activated");
        isFeedbackActive = true;
        while(isFeedbackActive)
        {
            feedBackText.color = accentColor;
            yield return new WaitForSeconds(1);
            feedBackText.color = baseColor;
            yield return new WaitForSeconds(1);
        }

        yield return null;
    }
}