using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TutorialManager))]
public class TutorialManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (!GUILayout.Button("Collect Tutorial Steps"))
            return;
        
        var steps = FindObjectsOfType<TutorialStep>().ToList();
        steps.Sort();
        ((TutorialManager)target).steps = steps.ToArray();
    }
}
