
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context media as public properties.
    /// </summary>
    public sealed class xAPI_Context_Media_Extensions {

        public xAPI_Context_Media_Extensions() {
        }
    }
}