using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class AbortGame : MonoBehaviour
{
    private static bool eventMuted = false;

    private void Start()
    {
        eventMuted = false;
    }
    public void OnExitPerformed()
    {
        //Only execute once for an active Scene
        if (eventMuted) return;
        eventMuted = true;

        Debug.Log("ExitPerformed Detected");

        var sceneID = SceneManager.GetActiveScene().buildIndex;

        if(sceneID == 0)
        {
            Application.Quit();
        }
        else
        {
            RunController.CurrentRunController.EndRun(1.5f);
        }
    }
}
