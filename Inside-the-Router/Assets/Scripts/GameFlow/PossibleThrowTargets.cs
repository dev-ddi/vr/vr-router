using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class can hold transform targets for package throws.
/// They are moved to a static singleton list on Start, so that every package script has access to these information
/// </summary>
public class PossibleThrowTargets : MonoBehaviour
{
    public static ThrowTargets targets;

    public Transform[] possibleTargets;

    private void Awake()
    {
        targets = new ThrowTargets();
    }

    private void Start()
    {
        targets.list.AddRange(possibleTargets);
    }
}

public class ThrowTargets { 
    public List<Transform> list = new();
}
