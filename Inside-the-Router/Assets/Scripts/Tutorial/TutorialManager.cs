using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Tutorial/Tutorial Manager")]
public class TutorialManager : MonoBehaviour
{
    public TutorialStep[] steps;
}
