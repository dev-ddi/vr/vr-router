using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

[CustomEditor(typeof(TutorialStep))]
public class TutorialStepEditor : Editor
{
    SerializedProperty tutorialStepClass;
    SerializedProperty text;
    SerializedProperty sound;
    SerializedProperty listenToAction;
    SerializedProperty spawnPackageConfiguration;
    SerializedProperty timeInSeconds;
    SerializedProperty provideSecondaryInstructions;
    SerializedProperty secondaryDelay;
    SerializedProperty secondaryText;
    SerializedProperty secondarySound;

    private void OnEnable()
    {
        // Setup the SerializedProperties.
        tutorialStepClass = serializedObject.FindProperty("Class");
        text = serializedObject.FindProperty("Text");
        sound = serializedObject.FindProperty("Sound");
        spawnPackageConfiguration = serializedObject.FindProperty("SpawnPackageConfiguration");
        timeInSeconds = serializedObject.FindProperty("TimeInSeconds");
        listenToAction = serializedObject.FindProperty("ListenToAction");
        provideSecondaryInstructions = serializedObject.FindProperty(nameof(TutorialStep.ProvideSecondaryInstructions));
        secondaryDelay = serializedObject.FindProperty(nameof(TutorialStep.SecondaryDelay));
        secondaryText = serializedObject.FindProperty(nameof(TutorialStep.SecondaryText));
        secondarySound = serializedObject.FindProperty(nameof(TutorialStep.SecondarySound));
    }

    public override void OnInspectorGUI()
    {
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();
        EditorGUILayout.PropertyField(tutorialStepClass);
        EditorGUILayout.Space(5f);

        TutorialStepClass stepClass = (TutorialStepClass)tutorialStepClass.enumValueIndex;

        switch (stepClass)
        {
            case TutorialStepClass.Timed:
                EditorGUILayout.PropertyField(text);
                EditorGUILayout.PropertyField(sound);
                EditorGUILayout.Space(5f);
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Wait Time"));
                break;
            case TutorialStepClass.ButtonPress:
                EditorGUILayout.PropertyField(text);
                EditorGUILayout.PropertyField(sound);
                EditorGUILayout.Space(5f);
                EditorGUILayout.PropertyField(listenToAction);
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Minimum Wait Time"));
                EditorGUILayout.PropertyField(provideSecondaryInstructions);
                if (provideSecondaryInstructions.boolValue) { 
                    EditorGUILayout.PropertyField(secondaryText);
                    EditorGUILayout.PropertyField(secondarySound);
                    EditorGUILayout.PropertyField(secondaryDelay);
                }
                break;
            case TutorialStepClass.SpawnPackage:
                EditorGUILayout.PropertyField(text);
                EditorGUILayout.PropertyField(sound);
                EditorGUILayout.Space(5f);
                EditorGUILayout.PropertyField(spawnPackageConfiguration);
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Wait Time"));

                break;
            case TutorialStepClass.DeliverPackage:
                EditorGUILayout.PropertyField(text);
                EditorGUILayout.PropertyField(sound);
                EditorGUILayout.Space(5f);
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Minimum Wait Time"));
                EditorGUILayout.PropertyField(provideSecondaryInstructions);
                if (provideSecondaryInstructions.boolValue)
                {
                    EditorGUILayout.PropertyField(secondaryText);
                    EditorGUILayout.PropertyField(secondarySound);
                    EditorGUILayout.PropertyField(secondaryDelay);
                }
                break;
            case TutorialStepClass.SpawnAndDeliverPackage:
                EditorGUILayout.PropertyField(text);
                EditorGUILayout.PropertyField(sound);
                EditorGUILayout.Space(5f);
                EditorGUILayout.PropertyField(spawnPackageConfiguration);
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Minimum Wait Time"));
                EditorGUILayout.PropertyField(provideSecondaryInstructions);
                if (provideSecondaryInstructions.boolValue)
                {
                    EditorGUILayout.PropertyField(secondaryText);
                    EditorGUILayout.PropertyField(secondarySound);
                    EditorGUILayout.PropertyField(secondaryDelay);
                }
                break;
            case TutorialStepClass.SendMessageToSceneController:
                EditorGUILayout.PropertyField(text, new GUIContent("Message"));
                EditorGUILayout.PropertyField(timeInSeconds, new GUIContent("Minimum Wait Time"));
                EditorGUILayout.PropertyField(provideSecondaryInstructions);
                if (provideSecondaryInstructions.boolValue)
                {
                    EditorGUILayout.PropertyField(secondaryText);
                    EditorGUILayout.PropertyField(secondarySound);
                    EditorGUILayout.PropertyField(secondaryDelay);
                }
                break;
        }


        /*
        // Show the custom GUI controls.
        EditorGUILayout.IntSlider(tutorialStepClass, 0, 100, new GUIContent("Damage"));

        // Only show the damage progress bar if all the objects have the same damage value:
        if (!tutorialStepClass.hasMultipleDifferentValues)
            ProgressBar(tutorialStepClass.intValue / 100.0f, "Damage");

        EditorGUILayout.IntSlider(text, 0, 100, new GUIContent("Armor"));

        // Only show the armor progress bar if all the objects have the same armor value:
        if (!text.hasMultipleDifferentValues)
            ProgressBar(text.intValue / 100.0f, "Armor");
        */

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
    }

    /*
    // Custom GUILayout progress bar.
    void ProgressBar(float value, string label)
    {
        // Get a rect for the progress bar using the same margins as a textfield:
        Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
        EditorGUI.ProgressBar(rect, value, label);
        EditorGUILayout.Space();
    }
    */
}
