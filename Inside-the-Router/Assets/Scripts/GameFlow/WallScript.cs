﻿using UnityEngine;

/// <summary>
/// This script is supposed to be attached to a wall. It respawns a package, if it collides with it
/// </summary>
public class WallScript : MonoBehaviour
{
    public BoundaryConfig BoundaryConfig;

    public static float XHigh;
    public static float XLow;
    public static float YHigh;
    public static float YLow;
    public static float ZHigh;
    public static float ZLow;

    private void Awake()
    {
        if (BoundaryConfig.XHigh) {
            XHigh = transform.position.x;
        }
        if (BoundaryConfig.XLow) { 
            XLow = transform.position.x;
        }
        if (BoundaryConfig.YHigh) { 
            YHigh = transform.position.y;
        }
        if (BoundaryConfig.YLow) {
            YLow = transform.position.y;
        }
        if (BoundaryConfig.ZHigh) {
            ZHigh = transform.position.z;
        }
        if (BoundaryConfig.ZLow) { 
            ZLow = transform.position.z;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var packageScript = other.gameObject.GetComponent<PackageScript>();

        if (packageScript != null)
        {
            packageScript.Respawn();
        }
    }
}

[System.Serializable]
public class BoundaryConfig {
    public bool XHigh = false;
    public bool XLow = false;
    public bool YHigh = false;
    public bool YLow = false;
    public bool ZHigh = false;
    public bool ZLow = false;
}
