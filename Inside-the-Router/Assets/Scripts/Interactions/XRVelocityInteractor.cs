using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// NOTE: component imported from InsideTheCpu-project
/// 
/// This MonoBehaviour samples the directions this object moved during the last x Physics frames and then calculates the mean velocity.
/// This is useful, as to approximate at which velocity the object is being moved and get rid of jitter.
/// </summary>
public class XRVelocityInteractor : MonoBehaviour
{
    /// <summary>
    /// This is the mean velocity over the last x physics frames
    /// </summary>
    public Vector3 velocity = Vector3.zero;
    public Vector3 rotation = Vector3.zero;

    private List<Vector3> SampledVelocities = new List<Vector3>();
    private List<Vector3> SampledRotations = new List<Vector3>();
    private static readonly int PhysicsFramesToSample = 8;
    private Vector3 positionLastFrame;
    private Vector3 rotationLastFrame;

    public void Reset()
    {
        SampledVelocities.Clear();
        SampledRotations.Clear();
    }

    void FixedUpdate()
    {
        SampledVelocities.Add((transform.position - positionLastFrame)/Time.fixedDeltaTime);
        SampledRotations.Add((transform.eulerAngles - rotationLastFrame)/Time.fixedDeltaTime);
        positionLastFrame = transform.position;
        rotationLastFrame = transform.eulerAngles;
        if (SampledVelocities.Count > PhysicsFramesToSample) {
            SampledVelocities.RemoveAt(0);
        }
        if (SampledRotations.Count > PhysicsFramesToSample)
        {
            SampledRotations.RemoveAt(0);
        }
        velocity = Vector3.zero;
        rotation = Vector3.zero;
        foreach (Vector3 v3 in SampledVelocities)
        {
            velocity += v3;
        }
        foreach (Vector3 v3 in SampledRotations)
        {
            rotation += v3;
        }
        velocity = new Vector3(velocity.x / SampledVelocities.Count, velocity.y / SampledVelocities.Count, velocity.z / SampledVelocities.Count);
        rotation = new Vector3(rotation.x / SampledRotations.Count, rotation.y / SampledRotations.Count, rotation.z / SampledRotations.Count);
    }
}
