
namespace xAPI.Registry {
    /// <summary>
    /// Provides the definitions of the context lms as public properties.
    /// </summary>
    public class xAPI_Context_Lms : xAPI_Context {
        /// <summary>
        /// 43 verbs of 'lms'.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs
        /// </summary>
        public readonly xAPI_Verbs_Lms verbs = new xAPI_Verbs_Lms();

        /// <summary>
        /// 32 activities of 'lms'.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/activities
        /// </summary>
        public readonly xAPI_Activities_Lms activities = new xAPI_Activities_Lms();

        /// <summary>
        /// 0 extensions of 'lms': .
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/extensions
        /// </summary>
        public readonly xAPI_Context_Lms_Extensions extensions = new xAPI_Context_Lms_Extensions();

        public xAPI_Context_Lms() 
            : base("lms") {
        }
    }
}