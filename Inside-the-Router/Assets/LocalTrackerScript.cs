using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LocalTrackerScript : MonoBehaviour
{
    public bool TrackingEnabled = true;

    private List<packageStatistic> RunStatistics;
    private packageStatistic currentStatistic;
    DateTime currentStart;
    DateTime currentEnd;
    

    private void Awake()
    {
        Run.CorrectPackagePlacement += OnCorrectPackagePlacement;
        Run.WrongPackagePlacement += OnWrongPackagePlacement;
        Run.RunStarted += OnRunStarted;
        Run.RunFinished += OnRunFinished;
    }

    private void OnRunFinished(object sender, RunEventArgs e)
    {
        var filename = DateTime.Now.ToString("MM-dd_HHmm") + ".csv";
        string path = Application.persistentDataPath + "/" + filename;
        StreamWriter writer = new StreamWriter(path, false);

        writer.WriteLine("Number;Time[s];Fails");

        foreach(var statistics in RunStatistics)
        {
            var line = statistics.Number + ";" + statistics.Time + ";";
            foreach(var fail in statistics.Fails)
            {
                line = line + fail;
            }

            writer.WriteLine(line);
        }

        writer.Close();
    }

    private void OnRunStarted(object sender, RunEventArgs e)
    {
        RunStatistics = new List<packageStatistic>();
        currentStatistic = CreatePackageStatisticForNextPackage();
    }

    private void OnWrongPackagePlacement(object sender, RunEventArgs e)
    {
        var number = Run.CurrentRun.Tubes.IndexOf(e.Tube as Tube);
        currentStatistic.Fails.Add(number);
    }

    private void OnCorrectPackagePlacement(object sender, RunEventArgs e)
    {
        currentEnd = DateTime.Now;
        TimeSpan duration = currentEnd - currentStart;
        currentStatistic.Time = duration.TotalSeconds;

        RunStatistics.Add(currentStatistic);
        currentStatistic = CreatePackageStatisticForNextPackage();
    }

    private packageStatistic CreatePackageStatisticForNextPackage()
    {
        var statistic = new packageStatistic();
        statistic.Number = RunStatistics.Count + 1;
        statistic.Fails = new List<int>();
        currentStart = DateTime.Now;

        return statistic;
    }
}
