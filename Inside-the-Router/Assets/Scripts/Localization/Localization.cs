﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class handles the localization of your game. Call it like this: Localization.Get.Phrase("your_key") to translate your keys.
/// 
/// Taken from Karl's personal scripts, hereby transferred to the public domain ;)
/// </summary>
public class Localization
{
    /// <summary>
    /// the Instance of the Localization.
    /// </summary>
    public static readonly Localization Get = new Localization();

    public readonly string[] SupportedLanguages = { "english_US", "german" };
    public readonly string[] SupportedLanguageClearnames = { "English (US)", "Deutsch" };
    private readonly string languageDirectoryName = "SupportedLanguages";
    public string CurrentLanguageClearname { get; private set; }
    public int CurrentLanguageIndex { get; private set; }
    private Dictionary<string, string> CurrentLanguage = new Dictionary<string, string>();
    private List<ILocalizable> RegisteredObjects = new List<ILocalizable>();
    public UnityEvent OnLanguageChanged = new();

    public Localization()
    {
        if (PlayerPrefs.HasKey("language"))
        {
            LoadLanguage(PlayerPrefs.GetString("language"));
        }
        else
        {
            LoadLanguage("english_US");
        }
    }

    public int LanguageToInt(string LanguageKey)
    {
        for (int i = 0; i < SupportedLanguages.Length; i++)
        {
            if (SupportedLanguages[i] == LanguageKey)
            {
                return i;
            }
        }
        return 0;
    }

    public void RegisterLocalizable(ILocalizable ToRegister)
    {
        RegisteredObjects.Add(ToRegister);
        SyncObject(ToRegister);
    }


    public void UpdateAllLocalization()
    {
        foreach (ILocalizable ToUpdate in RegisteredObjects)
        {
            SyncObject(ToUpdate);
        }
        OnLanguageChanged?.Invoke();
    }

    public void SyncObject(ILocalizable ToUpdate)
    {
        ToUpdate.SyncLocalization(Phrase(ToUpdate.GetKey()));
    }

    public void LoadLanguage(string name, bool FixRecursiveCall = false)
    {
        if (Application.isEditor && !FixRecursiveCall)
        {
            //if in editor, use more performance heavy but safer method
            CheckLanguageFile(name);
            return;
        }
        PlayerPrefs.SetString("language", name);
        CurrentLanguage.Clear();
        Debug.Log("Loading language at: " + languageDirectoryName + "/" + name);
        string current = Resources.Load<TextAsset>(languageDirectoryName + "/" + name).text;
        current = current.TrimStart('{');
        current = current.TrimEnd('}');
        current = current.Replace(System.Environment.NewLine, "");
        string[] keysAndValues = current.Split('"');
        for (int i = 1; i < keysAndValues.Length; i += 4)
        {
            AddLineToLanguage(keysAndValues, i);
        }
        CurrentLanguageClearname = Phrase("language_name");
    }

    public List<string> CompleteKeyList = new List<string>();

    public void CheckLanguageFile(string name)
    {
        if (CompleteKeyList.Count == 0)
        {
            LoadLanguage("german", FixRecursiveCall: true);
            foreach (KeyValuePair<string, string> kvp in CurrentLanguage)
            {
                CompleteKeyList.Add(kvp.Key);
            }
        }

        PlayerPrefs.SetString("language", name);
        CurrentLanguage.Clear();
        Debug.Log("Loading language at: " + languageDirectoryName + "/" + name);
        string current = Resources.Load<TextAsset>(languageDirectoryName + "/" + name).text;
        current = current.TrimStart('{');
        current = current.TrimEnd('}');
        current = current.Replace(System.Environment.NewLine, "");
        string[] keysAndValues = current.Split('"');
        int lineIndex = 0;
        for (int i = 1; i < keysAndValues.Length; i += 4)
        {
            try
            {
                if (CurrentLanguage.ContainsKey(keysAndValues[i]) == false)
                {
                    AddLineToLanguage(keysAndValues, i);
                }
                else
                {
                    Debug.LogError("double key in language: " + keysAndValues[i]);
                    Debug.LogError("value to this is: " + keysAndValues[i + 2]);
                    string line = keysAndValues[i] + keysAndValues[i + 1] + keysAndValues[i + 2] + keysAndValues[i + 3];
                    Debug.LogError("Line: " + line);
                    break;
                }
            }
            catch (System.Exception E)
            {
                Debug.LogError(E.GetType() + " near line: " + lineIndex);
                string line = "";
                foreach (string part in keysAndValues)
                {
                    line += part;
                }
                Debug.LogError("Line: " + line);
                throw;
            }

            lineIndex++;
        }

        string MissingKeys = "";
        foreach (string RequiredKey in CompleteKeyList)
        {
            if (!CurrentLanguage.ContainsKey(RequiredKey))
            {
                MissingKeys += "\"" + RequiredKey + "\": " + "\"\"" + ",\n";
            }
        }
        if (MissingKeys.Length != 0)
        {
            Debug.LogError("Missing Keys: ");
            Debug.LogError(MissingKeys);
        }
        CurrentLanguageClearname = Phrase("language_name");
    }

    private void AddLineToLanguage(string[] keysAndValues, int i)
    {
        CurrentLanguage.Add(keysAndValues[i], keysAndValues[i + 2].Replace("\'\'\'", "\"").Replace("\\n", "\n").Replace("<style='value'>", "<style=\"value\">").Replace("<style='key'>", "<style=\"key\">"));
    }

    /// <summary>
    /// translates a key
    /// </summary>
    /// <param name="key">the key under which the needed string is saved in the language file.</param>
    public string Phrase(string key)
    {
        if (CurrentLanguage.ContainsKey(key))
            return CurrentLanguage[key];

        Debug.LogWarning("Requested key " + key + " not available.");
        return key;
    }

    public void PrintLanguageToConsole()
    {
        Debug.Log("Printing current language: ");
        foreach (string key in CurrentLanguage.Keys)
        {
            Debug.Log(key + " : " + CurrentLanguage[key]);
        }
    }

}
