﻿using TMPro;
using UnityEngine;

public class LocalizedText : MonoBehaviour, ILocalizable
{
    public bool ReadKeyFromText = true;
    [Header("Settings")]
    [Tooltip("The key with which this text will be identified in the language manager")]
    public string LanguageKey;
    [Header("Overwrite default target")]
    [Tooltip("This Text will be used, instead of the first one found with GetComponent()")]
    public TMP_Text Text;

    private void Awake()
    {
        if (Text == null)
        {
            Text = GetComponent<TMP_Text>();
        }
        if (ReadKeyFromText) {
            LanguageKey = Text.text;
        }
    }

    private void Start()
    {
        Localization.Get.RegisterLocalizable(this);
    }

    public void SyncLocalization(string value)
    {
        Text.text = value;
    }

    public string GetKey()
    {
        return LanguageKey;
    }

    public string DebugErrorPrintName()
    {
        return name;
    }

}
