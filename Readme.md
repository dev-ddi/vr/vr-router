# Inside the Router

Inside the Router ist eine VR-Lernanwendung zum Thema Routing in Heimnetzwerken mit einem besonderen Fokut auf Network-Adress-Translation (NAT). Weitere Infos zum Gamedesign und dem Spielhinhalt können [hier](https://eleed.campussource.de/archive/14/5521) gefunden werden.

## Benutzung

Für die Benutzung wird [Unity](https://unity3d.com) in der Version 2019.4.20f1 benötigt. Inside the Router nutzt SteamVR und ist somit mit den meisten VR-Headsets kompatibel (getestet HTC Vive Pro, Pico Neo 3 im Link Modus). 

Das Unity-Projekt beinhaltet Szenen (`/Assets/Scenes`) mit verschiedenen Varianten des Spiels:
* **Free Game:** Die Szene beinhaltet das Basisspiel bei dem Zufällige Pakete zugeordnet werden müssen.
* **Demo01:** Die Szene beinhaltet das Spiel, bei dem die Steuerung mit einem [Dashboard](https://gitlab.com/ddi-tu-dresden/vr/vr-router-dashboard) ermöglicht wird.
* **Study01:** Die Szene beinhaltet die Spielkonfiguration die zur Erhebung von Lerndaten während einer ersten Studie genutzt wurde.

## Lerndaten

Inside the Router erhebt Lerndaten im xApi-Format mithilfe des [EduXR Frameworks](https://gitlab.com/learntech-rwth/eduxr/).
 


Versionen für android-basierte VR-Systeme werden aktuell entwickelt.

## Lizenz
Inside the Router steht unter der AGPLv3 Lizenz.

## Förderhinweis
Inside the Router wurde in Zusammenarbeit mit dem Projekt PraxisdigitaliS entwickelt.
PraxisdigitaliS wird im Rahmen der gemeinsamen „Qualitätsoffensive Lehrerbildung“ von Bund und Ländern aus Mitteln des Bundesministeriums für Bildung und Forschung gefördert.
