using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[AddComponentMenu("Tutorial/Tutorial Step")]
[System.Serializable]
public class TutorialStep : MonoBehaviour, IComparable<TutorialStep>
{
    public TutorialStepClass Class;
    [TextArea]
    public string Text;
    public AudioClip Sound;
    public bool ProvideSecondaryInstructions;
    public float SecondaryDelay = 10f;
    [TextArea]
    public string SecondaryText;
    public AudioClip SecondarySound;

    public float TimeInSeconds;
    public InputActionReference[] ListenToAction;
    public Difficulty SpawnPackageConfiguration;

    public int CompareTo(TutorialStep obj)
    {
        return name.CompareTo(obj.name);
    }
}

public enum TutorialStepClass { 
    Timed, ButtonPress, SpawnPackage, DeliverPackage, SpawnAndDeliverPackage, SendMessageToSceneController
}

