using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// using a singleton pattern, this class makes sure it is the only instance of itself. You can safely place containers in the menu and every scene,
/// only the first ever enabled container will persist.
/// It is made persistent over scene transitions.
/// It also searches for every class of type PersistantBehaviour in itself or any of its children and calls the setUp method so that these objects can
/// be sure, that they are executed on the only true persistent Container.
/// </summary>
public class PersistentContainer : MonoBehaviour
{
    public static PersistentContainer Instance;

    private void OnEnable()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            foreach (PersistantBehaviour behaviour in GetComponentsInChildren<PersistantBehaviour>())
            {
                behaviour.SetUp();
            }
        }
        else {
            Destroy(gameObject);
        }
    }

    private void OnDisable()
    {
        if (Instance == this) { 
            Instance = null;
        }
    }
}
