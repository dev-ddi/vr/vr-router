﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public void DestroyObj(XRBaseInteractable interactable)
    {
        Destroy(interactable.gameObject);
    }
}
