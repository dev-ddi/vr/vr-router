﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class NatTableTest
    {
        [Test]
        public void GenerateNatTableTest()
        {
            //Generate some Tubes
            List<Tube> tubes = new List<Tube>
            {
                new Tube(){ IpAddress = 0b_00000001_00000001_00000001_00000001 },    //Intern 
                new Tube(){ IpAddress = 0b_00000001_00000001_00000001_00000010 },    //Intern
                new Tube(){ IpAddress = 0b_00000001_00000001_00000001_00000100 },    //Intern
                new Tube(){ IpAddress = 0b_00000010_00000010_00000010_00000010 }     //Gateway
            };

            //Make some shortcuts to make manual package creation easier
            Address extern1 = new Address(0b_00000010_00000010_00000100_00000100, 81);
            Address extern2 = new Address(0b_00000010_00000111_00000100_00000000, 21);

            //Create some Packages like we would expect in a game
            List<Package> packages = new List<Package>
            {
                //Outgoing
                new Package(new Address(tubes[0].IpAddress, 81), extern1),
                new Package(new Address(tubes[0].IpAddress, 80), extern1),
                new Package(new Address(tubes[1].IpAddress, 81), extern1),
                new Package(new Address(tubes[2].IpAddress, 21), extern2), 

                //Incoming
                new Package(extern1, new Address(tubes[0].IpAddress, 81)),
                new Package(extern2, new Address(tubes[1].IpAddress, 80)),
            };

            //Create the NatTableEntries we would need to send these Packages
            NatTableEntry NAT1 = new NatTableEntry() { ExternalAddress = new Address(tubes[3].IpAddress, 6000), InternalAddress = packages[0].Source };
            NatTableEntry NAT2 = new NatTableEntry() { ExternalAddress = new Address(tubes[3].IpAddress, 6001), InternalAddress = packages[2].Source };
            NatTableEntry NAT3 = new NatTableEntry() { ExternalAddress = new Address(tubes[3].IpAddress, 6002), InternalAddress = packages[3].Source };

            //Act
            var Entries = NatTable.GenerateTableEntries(packages, tubes, tubes[3].IpAddress);

            //Check
            Assert.AreEqual(2, Entries.Count);

            Assert.AreEqual(NAT1.ExternalAddress.Port, Entries[0].ExternalAddress.Port);
            Assert.AreEqual(NAT2.ExternalAddress.Port, Entries[1].ExternalAddress.Port);

            Assert.AreEqual(NAT1.InternalAddress.IpAddress, Entries[0].InternalAddress.IpAddress);
            Assert.AreEqual(NAT2.InternalAddress.IpAddress, Entries[1].InternalAddress.IpAddress);
        }
    }
}
