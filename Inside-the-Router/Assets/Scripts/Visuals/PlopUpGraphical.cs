using System.Collections;
using UnityEngine;

/// <summary>
/// Attach this component to a sprite to control its size via a curve. Use this e.g. for a nice spawn effect.
/// </summary>
public class PlopUpGraphical : MonoBehaviour
{
    [System.Serializable]
    public class AnimationStackEntry
    {
        [Tooltip("Scale modifier over time")] public AnimationCurve SizeOverTime;

        [Tooltip("Total time the animation will take")]
        public float TimeTotal;

        [Tooltip("Snaps to the standard scale at end of animation")]
        public bool SnapToBaseSize = true;
    }

    [Tooltip("Specify animation curves")] public AnimationStackEntry[] AnimationStack;

    [Tooltip("Set to true to avoid timeScale affecting the animation")]
    public bool unscaledTime = false;

    [Tooltip("Play animation at index 0 on start")]
    public bool PlayOnStart = true;

    private void Start()
    {
        if (PlayOnStart)
        {
            PlayAnimation(0);
        }
    }

    /// <summary>
    /// Plays the animation from the array at the given index.
    /// </summary>
    /// <param name="index">The index to play.</param>
    public void PlayAnimation(int index)
    {
        if (index < AnimationStack.Length)
        {
            StartCoroutine(PlopUpRoutine(index));
        }
        else Debug.LogWarning("Animation at Index " + index + " not found.", gameObject);
    }

    /// <summary>
    /// Routine that samples form the animation Curve
    /// </summary>
    /// <param name="index">The animation curve to be played</param>
    private IEnumerator PlopUpRoutine(int index)
    {
        Vector3 endSize = transform.localScale;
        float startTime = unscaledTime ? Time.realtimeSinceStartup : Time.time;
        while ((unscaledTime ? Time.realtimeSinceStartup : Time.time) < startTime + AnimationStack[index].TimeTotal)
        {
            transform.localScale = endSize *
                                   AnimationStack[index].SizeOverTime.Evaluate
                                   (((unscaledTime ? Time.realtimeSinceStartup : Time.time) - startTime) /
                                    AnimationStack[index].TimeTotal);
            yield return null;
        }

        transform.localScale = AnimationStack[index].SnapToBaseSize
            ? endSize
            : endSize * AnimationStack[index].SizeOverTime.Evaluate(1f);
    }
}