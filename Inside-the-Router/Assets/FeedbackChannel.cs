using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum FeedbackCase
{
    UNKNOWN,

    INTERNAL_PACKAGE_TRIES,
    OUTGOING_PACKAGE_TRIES,
    INCOMING_PACKAGE_TRIES,

    INTERNAL_PACKAGE_TIME,
    OUTGOING_PACKAGE_TIME,
    INCOMING_PACKAGE_TIME
}

public abstract class FeedbackChannel: MonoBehaviour
{
    private FeedbackManager feedbackManager = null;


    /// <summary>
    /// Give Feedback during game by highlighting relevant information visually or displaying additional help.
    /// Feedback can vary regarding the case of feedback and the type of object implementing the IFeedbackGiver Interface.
    /// </summary>
    /// <param name="fbc"></param>
    public abstract void OnFeedbackRequested(FeedbackCase fbc);

    protected virtual void Start()
    {
        RegisterAtFeedbackManager();
        ActivateInCaseOfOngoingFeedback();
    }

    /// <summary>
    /// Register this Component at your LiveFeedbackManager in order to get notified aboud neccessary feedback
    /// </summary>
    private void RegisterAtFeedbackManager()
    {
        var fbm = GameObject.FindGameObjectWithTag("FeedbackManager");

        if(fbm != null)
        {
            feedbackManager = fbm.GetComponent<FeedbackManager>();
            feedbackManager.RegisterFeedbackChannel(this);
        }
    }


    protected virtual void OnDestroy()
    {
        UnregisterAtFeedbackManager();
    }

    /// <summary>
    /// Let your LiveFeedback Manager know when you dont want to be notified about future feedback requests,
    /// especially if this component will get destroyed!
    /// </summary>
    private void UnregisterAtFeedbackManager()
    {
        if(feedbackManager != null)
        {
            feedbackManager.UnregisterFeedbackChannel(this);
        }
    }

    private void ActivateInCaseOfOngoingFeedback()
    {
        if(feedbackManager != null)
        {
            if (feedbackManager.isFeedbackCurrentlyActive) OnFeedbackRequested(FeedbackCase.UNKNOWN);
        }
    }

}
