using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartArguments : PersistantBehaviour
{
    public static StartArguments Instance;

    public enum StartMode { 
        RANDOM, TUTORIAL, STUDY
    }

    public enum LenghtMode
    {
        TIME, PACKAGE_COUNT, UNLIMITED, OVERFLOW
    }

    public enum Version
    {
        THROWING, WALKING
    }


    public StartMode mode;
    public Version version;

    public LenghtMode lenghtMode;
    public float TimeInMinutes;
    public int PackageCount;
    public int MaxPackages;
    public int Language;


    public override void SetUp()
    {
        Instance = this;
    }

}
