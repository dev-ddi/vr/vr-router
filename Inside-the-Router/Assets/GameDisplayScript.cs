using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameDisplayScript : FeedbackChannel
{
    public TMP_Text header;
    public TMP_Text body;
    public Image headerBackground;
    public Canvas canvas;

    private bool isFeedbackActive = false;
    private string NatTableString = "";

    // Start is called before the first frame update
    protected override void Start()
    {
        header.text = "";
        body.text = "";
        canvas.worldCamera = GameObject.FindGameObjectsWithTag("MainCamera")[0].GetComponent<Camera>();
        Run.RunStarted += OnRunStarted;
        Run.RunFinished += OnRunFinished;
        Run.CorrectPackagePlacement += CorrectPackageReceived;

        base.Start();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        Run.RunStarted -= OnRunStarted;
        Run.RunFinished -= OnRunFinished;
        Run.CorrectPackagePlacement -= CorrectPackageReceived;
    }

    private void OnRunStarted(object sender, RunEventArgs e)
    {
        ShowCurrentNatInfo();
    }

    private void OnRunFinished(object sender, RunEventArgs e)
    {
        isFeedbackActive = false;
        ShowEndScreen();
    }

    private void CorrectPackageReceived(object sender, RunEventArgs e)
    {
        isFeedbackActive = false;
        body.text = NatTableString;
    }

    private void ShowEndScreen()
    {
        header.text = "Game Over";
        body.text = "";
        headerBackground.color = new Color(0,0,0,0);
    }

    private void ShowCurrentNatInfo()
    {
        foreach (var entry in NatTable.Entries)
        {
            var extraSpace = "";
            if (entry.InternalAddress.Port < 100)
            {
                extraSpace = " ";
            }

            NatTableString +=
                $"  {entry.InternalAddress.StrAddress} |     {extraSpace}{entry.InternalAddress.Port}     |   {entry.ExternalAddress.Port}\n";
        }

        body.text = NatTableString;
        header.text = " Client IP  | Client Port | Externer Port";
    }


    [ContextMenu("Activate Feedback")]
    public void activateFeedback()
    {
        OnFeedbackRequested(FeedbackCase.UNKNOWN);
    }

    [ContextMenu("Deactivate Feedback")]
    public void DeactivateFeedback()
    {
        isFeedbackActive = false;
    }

    public override void OnFeedbackRequested(FeedbackCase fbc)
    {
        HighlightMatchingNatEntry();
    }

    private void HighlightMatchingNatEntry() 
    {
        var package = RunController.CurrentRunController.CurrentPackages[0];
        var dataPackage = package.GetDataPackage();

        //Only Feedback for incoming packets
        if (dataPackage.Destination.IpAddress == IpUtils.ExternalRouterIP)
        {
            foreach (var entry in NatTable.Entries)
            {
                if (entry.ExternalAddress.Port == dataPackage.Destination.Port )
                {
                    isFeedbackActive = true;
                    StartCoroutine(BlinkNatRow(NatTable.Entries.IndexOf(entry)));
                    return;
                }
            }
        }       
    }


    private IEnumerator BlinkNatRow(int rowToBlink)
    {
        var highlightedTable = "";
        string[] tableLines = body.text.Split("\n");

        for (int i = 0; i < tableLines.Length; i++)
        {
            if (rowToBlink == i)
            {
                highlightedTable += "<color=\"green\">" + tableLines[i] + "</color>\n";
            }
            else
            {
                highlightedTable += tableLines[i]+"\n";
            }
        }

        while (isFeedbackActive)
        {
            body.text = highlightedTable;
            yield return new WaitForSeconds(1);

            body.text = NatTableString;
            yield return new WaitForSeconds(1);
        }

        yield return null;
    }
    
}

