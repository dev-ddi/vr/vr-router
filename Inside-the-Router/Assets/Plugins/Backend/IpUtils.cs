using System;
using System.Collections.Generic;

public static class IpUtils
{
    public static UInt32 ExternalRouterIP = GetUInt32FromIPAddress("142.67.42.5");
    public static UInt32 InternalRouterIP = GetUInt32FromIPAddress("192.168.0.1");
    public static UInt32 IspIP = GetUInt32FromIPAddress("142.67.42.1");

    private static byte ipCounter = 2;

    public static void ResetIpCounter() {
        ipCounter = 2;
    }

    public static UInt32 CreateInternalIPAddress()
    {
        var ip = IpUtils.GetUInt32FromIPAddress("192.168.0." + ipCounter);
        ipCounter++;
        return ip;
    }

    public static UInt32 GetUInt32FromIPAddress(string address)
    {
        string[] addressparts = address.Split('.');
        byte[] WANIPByteArray = new byte[4];
        WANIPByteArray[0] = BitConverter.GetBytes(int.Parse(addressparts[3]))[0];
        WANIPByteArray[1] = BitConverter.GetBytes(int.Parse(addressparts[2]))[0];
        WANIPByteArray[2] = BitConverter.GetBytes(int.Parse(addressparts[1]))[0];
        WANIPByteArray[3] = BitConverter.GetBytes(int.Parse(addressparts[0]))[0];
        return BitConverter.ToUInt32(WANIPByteArray, 0);
    }

    public static Address GenerateRandomExternalAddress()
    {
        Random rnd = new Random();
        Address ipAddress = new Address();
        ipAddress.IpAddress = GenerateRandomExternalIP();
        ipAddress.Port = BitConverter.ToUInt16(BitConverter.GetBytes(rnd.Next(21, 300)), 0);
        return ipAddress;
    }

    public static UInt32 GenerateRandomExternalIP()
    {
        Random rnd = new Random();
        List<string> available = new List<string>()
        {
            "172.217.20.109",   //Google
            "66.220.149.25",    //Facebook
            "208.65.153.238"    //Youtube
        };

        string strAddr = available[rnd.Next(0, 3)];
        return GetUInt32FromIPAddress(strAddr);
    }

    public static string ConvertUIntToString(UInt32 address)
    {
        // Convert unsigned int to byte array
        byte[] bytes = BitConverter.GetBytes(address);
        // reverse order of output
        return $"{bytes[3]}.{bytes[2]}.{bytes[1]}.{bytes[0]}";
    }

    public static string ConvertAddressToString(Address address)
    {
        return $"{ConvertUIntToString(address.IpAddress)}:{address.Port}";
    }
}