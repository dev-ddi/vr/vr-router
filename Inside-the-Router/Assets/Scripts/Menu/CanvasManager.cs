using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CanvasManager : MonoBehaviour
{
    public GameObject SettingsCanvas;
    public InputActionReference MenuAction;

    private void OnEnable()
    {
        MenuAction.action.performed += Toggle;
    }

    private void OnDisable()
    {
        MenuAction.action.performed -= Toggle;
    }

    private void Start()
    {
        SettingsCanvas.gameObject.SetActive(false);
    }

    private void Toggle(InputAction.CallbackContext context) {
        SettingsCanvas.gameObject.SetActive(!SettingsCanvas.gameObject.activeSelf);
    }

}
