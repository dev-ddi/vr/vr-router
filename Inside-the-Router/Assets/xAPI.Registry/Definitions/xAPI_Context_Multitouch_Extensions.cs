
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context multitouch as public properties.
    /// </summary>
    public sealed class xAPI_Context_Multitouch_Extensions {

        public xAPI_Context_Multitouch_Extensions() {
        }
    }
}