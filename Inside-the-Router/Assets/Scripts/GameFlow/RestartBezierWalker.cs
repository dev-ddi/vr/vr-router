﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class RestartBezierWalker : MonoBehaviour
{
    public GameObject dropZone;
    private BezierSolution.BezierWalkerWithSpeed bezierWalker;

    private void Start()
    {
        bezierWalker = GetComponent<BezierSolution.BezierWalkerWithSpeed>();
    }
    public void RestartBezier()
    {
        //Disable Drop Zone, so package does not stay there after bezier walker animation
        dropZone.GetComponent<XRSocketInteractor>().enabled = false;
        dropZone.GetComponent<SphereCollider>().enabled = false;
        // Start Bezierwalker from 0 progress
        bezierWalker.NormalizedT = 0.00f;
        bezierWalker.enabled = true;
    }

    public void PathFinished()
    {
        // Disable bezierwalker
        bezierWalker.enabled = false;
        // Reenable drop zone for throwing
        dropZone.GetComponent<XRSocketInteractor>().enabled = true;
        dropZone.GetComponent<SphereCollider>().enabled = true;
    }
}
