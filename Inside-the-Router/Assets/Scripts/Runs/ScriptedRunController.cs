using System;
using System.Collections;
using System.Linq;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;

public class ScriptedRunController : RunController
{
    private int currentLevel = 1;

    protected virtual void Start()
    {
        Run.RunStarted += OnRunStarted;
        LrsController.Instance.StartTracking();
        InitializeRun();
    }

    void OnDestroy()
    {
        Run.RunStarted -= OnRunStarted;
    }

    private void OnRunStarted(object sender, RunEventArgs e)
    {
        SpawnNextPackage();
    }

    public override void InitializeRun()
    {
        CurrentRunController = this;
        IpUtils.ResetIpCounter();

       
        Run run = new ScriptedRun(5);

        //Match Tubes between Front- and Backend
        if (UiTubes.Count != tubeCount)
            throw new System.Exception("Number of Tubes differes between Front- and Backend");
        for (int i = 0; i < tubeCount; i++)
        {
            UiTubes[i].GetComponent<TubeScript>().SetTube(run.Tubes[i]);
        }

        SubscribeToTubeEvents();
        SubscribeTrackingEvents();
        StartCoroutine(DelayedStart(run, 2));
    }

    IEnumerator DelayedStart(Run run, float delay)
    {
        yield return new WaitForSeconds(delay);

        run.Start();
        StartTime = Time.time;
    }

    private void SubscribeTrackingEvents()
    {
        //Deactivate Tracking for now
        return;
        Run.RunStarted += TrackRunStarted;
        Run.RunFinished += TrackRunFinished;
        Run.CorrectPackagePlacement += TrackCorrectPackage;
        Run.WrongPackagePlacement += TrackWrongPackage;
    }

    private void TrackWrongPackage(object sender, RunEventArgs e)
    {
        Debug.Log("WrongPackage");
        var verbs = new xAPI.Registry.xAPI_Verbs_VirtualReality();
        var activity = new xAPI.Registry.xAPI_Activities_VirtualReality();
        var extension = new xAPI.Registry.xAPI_Extensions_Activity_VirtualReality();
        extension.uiElementValue(currentLevel);
        extension.vrObjectName("wrong");
        LrsController.Instance.SendStatement(verbs.placed, activity.vrObject, extension);
    }

    private void TrackCorrectPackage(object sender, RunEventArgs e)
    {
        Debug.Log("CorrectPackage");
        var verbs = new xAPI.Registry.xAPI_Verbs_VirtualReality();
        var activity = new xAPI.Registry.xAPI_Activities_VirtualReality();
        var extension = new xAPI.Registry.xAPI_Extensions_Activity_VirtualReality();
        extension.uiElementValue(currentLevel);
        extension.vrObjectName("correct");
        
        
        LrsController.Instance.SendStatement(verbs.placed, activity.vrObject, extension);
        currentLevel++;
    }

    private void TrackRunFinished(object sender, RunEventArgs e)
    {
        Debug.Log("Done");
        var verbs = new xAPI.Registry.xAPI_Verbs_SystemControl();
        var activity = new xAPI.Registry.xAPI_Activities_SeriousGames();

        LrsController.Instance.SendStatement(verbs.finished, activity.game);
        LrsController.Instance.StopTracking();
    }

    private void TrackRunStarted(object sender, RunEventArgs e)
    {
        Debug.Log("Started");
        currentLevel = 1;

        var verbs = new xAPI.Registry.xAPI_Verbs_SystemControl();
        var activity = new xAPI.Registry.xAPI_Activities_SeriousGames();
        LrsController.Instance.SendStatement(verbs.loaded, activity.game);
        LrsController.Instance.SendStatement(verbs.initialized, activity.game);
        LrsController.Instance.SendStatement(verbs.started, activity.game);
    }

    protected override void SpawnNextPackage(int difficulty = 0)
    {
        Package package =(Run.CurrentRun as ScriptedRun).GetNextScriptedPackage();


        if (package != null)
        {
            //Select Source Tube
            Tube sourceTube = null;

            if (Run.CurrentRun.Tubes.Any(t => t.IpAddress == package.Source.IpAddress))
            {
                //Select Tube with matching IpAddress
                sourceTube = Run.CurrentRun.Tubes.First(t => t.IpAddress == package.Source.IpAddress);
            }
            else
            {
                //Select WAN Tube
                sourceTube = Run.CurrentRun.Tubes.Last();
            }

            SpawnPackageFromTube(package, sourceTube);
            FlashTubeForAttention(sourceTube);
        }
        else
        {
            EndRun();
        }
    }

    public override void EndRun(float delay = 5f)
    {
        base.EndRun(delay);
        UnsubscribeTrackingEvents();
    }

    private void UnsubscribeTrackingEvents()
    {
        //Deactivate tracking for now
        return;
        Run.RunStarted -= TrackRunStarted;
        Run.RunFinished -= TrackRunFinished;
        Run.CorrectPackagePlacement -= TrackCorrectPackage;
        Run.WrongPackagePlacement -= TrackWrongPackage;
    }

    [ContextMenu("BackToMainMenu")]
    private void BackToMainMenu()
    {
        EndRun(0);
    }
}
