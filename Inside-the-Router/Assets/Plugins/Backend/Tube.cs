﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public class Tube
{
    public UInt32 IpAddress { get; set; }
    public object Client { get; set; }

    public event EventHandler<RunEventArgs> CorrectPackageReceived;
    public event EventHandler<RunEventArgs> WrongPackageReceived;

    /// <summary>
    /// Call when there is a package placed in this tube.
    /// The Tube will process the Package and trigger the matching event.
    /// </summary>
    /// <param name="package"></param>
    public void HandlePackage(Package p)
    {
        //Direct Package correct received
        if (this.IpAddress == p.Destination.IpAddress)
        {
            CorrectPackageReceived?.Invoke(this, new RunEventArgs("Correct Direct", p, this));
        }
        //Incoming Package correct received
        else if (NatTable.Entries.Any(e => e.ExternalAddress.Equals((Address)p.Destination) && e.InternalAddress.IpAddress == this.IpAddress))
        {
            CorrectPackageReceived?.Invoke(this, new RunEventArgs("Correct Incoming", p, this));
        }
        //Outgoing Package correct received
        else if (Run.CurrentRun.Tubes.All(t => t.IpAddress != p.Destination.IpAddress) && IpUtils.ExternalRouterIP != p.Destination.IpAddress && Run.CurrentRun.Tubes.Last() == this)
        {
            CorrectPackageReceived?.Invoke(this, new RunEventArgs("Correct Outgoing", p, this));
        }
        //Direct Package wrong received
        else if (p.Destination.StrAddress.Contains("192.168."))
        {
            WrongPackageReceived?.Invoke(this, new RunEventArgs("Wrong Direct", p, this));
        }
        //Incoming Package wrong received
        else if (p.Destination.IpAddress == IpUtils.ExternalRouterIP)
        {
            WrongPackageReceived?.Invoke(this, new RunEventArgs("Wrong Incoming", p, this));
        }
        //Outgoing Package wrong received
        else
        {
            WrongPackageReceived?.Invoke(this, new RunEventArgs("Wrong Outgoing", p, this));
        }
    }


    /// <summary>
    /// Generates the given number of tubes, where all but the last tubes have "internal" IpAddresses.
    /// The last tube will have the gateway ipAddress
    /// </summary>
    /// <param name="tubeCount"></param>
    /// <returns></returns>
    public static List<Tube> CreateTubes(int tubeCount)
    {
        var tubes = new List<Tube>();

        //Internal Tubes
        for (int i = 0; i < tubeCount - 1; i++)
        {
            tubes.Add(new Tube()
            {
                Client = "Client" + i.ToString(),
                IpAddress = IpUtils.CreateInternalIPAddress()
            });
        }

        //WAN Port
        tubes.Add(new Tube()
        {
            Client = "WAN",
            IpAddress = IpUtils.IspIP
        });

        return tubes;
    }

}
