﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatusScript : MonoBehaviour
{
    public TMP_Text statusWallText;
    public TMP_Text runStatusText;
    public TMP_Text statusGroundText;
    public TMP_Text NAT_Header;

    private void Start()
    {
        Localization.Get.OnLanguageChanged.AddListener(ShowAll);
    }

    public void ShowAll()
    {
        Clear();
        ShowExternalIP();
        ShowIspInfo();
        ShowCurrentNatInfo();
    }

    private void Clear()
    {
        statusWallText.text = "";
        runStatusText.text = "";
        statusGroundText.text = "";
        //NAT_Header.text = "";
    }

    public void ShowRunStatus(string text)
    {
        runStatusText.text = text;
    }
    
    private void ShowExternalIP()
    {
        statusGroundText.text += "Inside the Router\n\n";
        statusGroundText.text +=
            $"Router (intern): {IpUtils.ConvertUIntToString(IpUtils.InternalRouterIP)}\n";
    }
    
    private void ShowIspInfo()
    {
        statusGroundText.text +=
            $"Router (extern): {IpUtils.ConvertUIntToString(IpUtils.ExternalRouterIP)}";
    }

    
    private void ShowCurrentNatInfo()
    {
        var natTableText = "\n-----------------------------------------\n";
        
        foreach (var entry in NatTable.Entries)
        {
            var extraSpace = "";
            if (entry.InternalAddress.Port < 100)
            {
                extraSpace = " ";
            }
            
            natTableText +=
                $"  {entry.InternalAddress.StrAddress} |   {entry.InternalAddress.Port}{extraSpace}   |   {entry.ExternalAddress.Port}\n";
        }

        statusWallText.text = natTableText;
    }

    public void ShowFinishText(string finishString)
    {
        statusWallText.text = $"{finishString}";
    }
}