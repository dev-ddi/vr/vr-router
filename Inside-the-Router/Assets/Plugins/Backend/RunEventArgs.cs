﻿using System;

public class RunEventArgs
{
    public String Type { get; set; }
    public object Package { get; set; }

    /// <summary>
    /// Used for actual Tube in a "Hit" Event
    /// </summary>
    public object Tube { get; set; }

    /// <summary>
    /// Used for correct Tube in a "Hit" Event
    /// </summary>
    public object Extra { get; set; }


    public RunEventArgs(String type, object package = null, object tube = null, object extra = null)
    {
        Type = type;
        Package = package;
        Tube = tube;
        Extra = extra;
    }
}
