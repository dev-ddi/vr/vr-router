using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuSelectTubeScript : MonoBehaviour
{
    public UnityEvent Activated;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("SelectorPackage"))
        {
            Activated.Invoke();
            Destroy(other);
        }
    }
}
