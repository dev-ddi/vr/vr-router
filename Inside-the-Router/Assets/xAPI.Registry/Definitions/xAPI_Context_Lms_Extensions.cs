
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context lms as public properties.
    /// </summary>
    public sealed class xAPI_Context_Lms_Extensions {

        public xAPI_Context_Lms_Extensions() {
        }
    }
}