using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Place this script somewhere on your XR-Rig and use it to toggle if the rig uses the direct interactors or the ray interactors
/// </summary>
public class ToggleInteractorMode : MonoBehaviour
{
    public bool enabled = true;

    public GameObject[] RayInteractors;
    public MonoBehaviour[] DirectInteractors;

    public void Toggle(bool RayActivated) {
        if (!enabled) return;   
        foreach (var rayIn in RayInteractors)
        {
            rayIn.SetActive(RayActivated);
        }
        foreach (var dirIn in DirectInteractors)
        {
            dirIn.enabled = !RayActivated;
        }
    }
}
