
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context observation as public properties.
    /// </summary>
    public sealed class xAPI_Context_Observation_Extensions {

        public xAPI_Context_Observation_Extensions() {
        }
    }
}