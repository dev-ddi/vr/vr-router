using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Animui
{
    /// <summary>
    /// helpfull methods for blending color and/or alpha values
    /// </summary>
    public class Blending
    {

        static Dictionary<CanvasGroup, BlendingDummyClass> runningBlendRoutines = new Dictionary<CanvasGroup, BlendingDummyClass>();
        /// <summary>
        /// takes a reference to a canvas group an blends the alpha value of this canvas group to the desired value
        /// </summary>
        /// <param name="canvasGroup">a reference to the canvas group</param>
        /// <param name="targetAlpha">the alpha value to be blended to</param>
        /// <param name="BlendTime">the time the process should take</param>
        /// <param name="IgnoreTimeScale">true if the calculations should be performed in realtime</param>
        public static void BlendCanvasGroupAlpha(CanvasGroup canvasGroup, float targetAlpha, float BlendTime, bool IgnoreTimeScale) {
            if (canvasGroup == null) Debug.LogError("Cannot blend alpha values as passed canvasGroup is <b>Null Pointer</b>");


            //if another routine is already blending on this canvas group, stop the other routine... forcefully
            if (runningBlendRoutines.ContainsKey(canvasGroup)) {
                runningBlendRoutines[canvasGroup].DestroyMe();
                runningBlendRoutines.Remove(canvasGroup);
            }

            //if the alpha is already at the desired value, skip everything
            if (canvasGroup.alpha == targetAlpha) return;

            //if Blend Time is 0 just set
            if (BlendTime == 0f) {
                canvasGroup.alpha = targetAlpha;
                return;
            }


            //else start blend process
            GameObject newObj = new GameObject("BlendingHelperObject");
            BlendingDummyClass newBDC = newObj.AddComponent<BlendingDummyClass>();
            newBDC.StartCoroutine(newBDC.BlendCanvasGroupAlphaRoutine(newObj, canvasGroup, targetAlpha, BlendTime, IgnoreTimeScale));
            runningBlendRoutines.Add(canvasGroup, newBDC);
        }

        class BlendingDummyClass : MonoBehaviour
        {
            GameObject me;
            bool meDead = false;

            public IEnumerator BlendCanvasGroupAlphaRoutine(GameObject me, CanvasGroup canvasGroup, float targetAlpha, float BlendTime, bool IgnoreTimeScale)
            {
                this.me = me;
                float startTime = IgnoreTimeScale ? Time.realtimeSinceStartup : Time.time;
                float startAlpha = canvasGroup.alpha;
                float a;
                while (!meDead && IgnoreTimeScale ? Time.realtimeSinceStartup < startTime + BlendTime : Time.time < startTime + BlendTime)
                {
                    a = Mathf.Lerp(startAlpha, targetAlpha, IgnoreTimeScale ? (Time.realtimeSinceStartup - startTime) / BlendTime : (Time.time - startTime) / BlendTime);
                    canvasGroup.alpha = a;
                    yield return null;
                }
                canvasGroup.alpha = targetAlpha;
                DestroyMe();
            }

            public void DestroyMe() {
                meDead = true;
                Destroy(me);
            }
        }
    }



}
