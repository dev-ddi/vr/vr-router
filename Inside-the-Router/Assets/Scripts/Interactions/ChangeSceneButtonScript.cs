﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneButtonScript : MonoBehaviour
{
    public SceneTransitionScript SceneTransition;

    public Material idleMaterial;
    public Material activeMaterial;
    public string sceneName;

    private MeshRenderer mr;

    void Start()
    {
        mr = gameObject.GetComponent<MeshRenderer>();
        mr.material = idleMaterial;
    }

    public void OnGrabStarted()
    {
        mr.material = activeMaterial;
        LoadScene();
    }

    public void OnGrabDone()
    {
    }

    private void LoadScene()
    {
        if (sceneName == "Run")
        {
            SceneTransition.OpenRunScene();
        }
        if (sceneName == "Tutorial")
        {
            SceneTransition.OpenTutorialScene();
        }
    }
}
