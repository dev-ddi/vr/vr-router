﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

/// <summary>
/// This script detects connected controllers and instantiates corresponding prefabs to the players hands
/// </summary>
public class ControllerPresentation : MonoBehaviour
{
    public InputDeviceCharacteristics controllerCharacteristics;
    public List<GameObject> controllerPrefabs;
    private InputDevice targetDevice;
    private GameObject spawnedController;

    void Start()
    {
        TryInitialize();
    }

    //TODO: potential bug: when reconnecting a controller or connecting a different controller,
    //a new model is spawned, but the old model is not destroyed. Test if this is the case!
    void TryInitialize()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);

        if (devices.Count > 0)
        {
            targetDevice = devices[0];
            GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name);
            if (prefab)
            {
                spawnedController = Instantiate(prefab, transform);
            }
            else
            {
                Debug.LogError("Couldn't find corresponding controller model.");
                spawnedController = Instantiate(controllerPrefabs[0], transform);
            }
        }
    }

    void Update()
    {
        if (!targetDevice.isValid)
        {
            TryInitialize();
        }
    }

}
