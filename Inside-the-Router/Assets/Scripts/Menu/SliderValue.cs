using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SliderValue : MonoBehaviour
{
    public TMP_Text text;
    public CanvasGroup cg;
    public bool fadeOut = true;
    public float fadeOutDelay = 0f;
    public string Addendum = " %";
    public string MaxValueOverwrite = "";
    public string MinValueOverwrite = "";
    public float MultiplySliderValue = 100f;
    public Slider slider;

    private void Start()
    {
        if (fadeOut)
        {
            cg.alpha = 0f;
        }
        else { 
            cg.alpha = 1f;
            if (slider != null)
            {
                SetText(slider.value);
            }
        }
        Localization.Get.OnLanguageChanged.AddListener(UpdateVisuals);
    }

    private void OnDestroy()
    {
        Localization.Get.OnLanguageChanged.RemoveListener(UpdateVisuals);
    }

    [ExecuteAlways]
    float lastUpdate = 0f;
    public void UpdateVisuals()
    {
        float newValue = slider.value;
        SetText(newValue);
        if (Application.isPlaying)
        {
            lastUpdate = Time.realtimeSinceStartup;
            Animui.Blending.BlendCanvasGroupAlpha(cg, 1f, 0f, true);
            if (fadeOut)
            {
                if (fadeOutDelay > 0f)
                {
                    StartCoroutine(FadeOutDelayed());
                }
                else
                    Animui.Blending.BlendCanvasGroupAlpha(cg, 0f, 2f, true);
            }
        }
        else
        {
            cg.alpha = 1f;
        }
    }

    private void SetText(float newValue)
    {
        if (slider != null && (int)newValue == slider.maxValue && MaxValueOverwrite != "")
        {
            text.text = Localization.Get.Phrase(MaxValueOverwrite);
        }
        else if (slider != null && (int)newValue == slider.minValue && MinValueOverwrite != "")
        {
            text.text = Localization.Get.Phrase(MinValueOverwrite);
        }
        else
        {
            text.text = $"{(int)(newValue * MultiplySliderValue)}{Localization.Get.Phrase(Addendum)}";
        }
    }

    public IEnumerator FadeOutDelayed() {
        yield return new WaitForSeconds(fadeOutDelay);
        if (Time.realtimeSinceStartup > lastUpdate + fadeOutDelay)
        {
            Animui.Blending.BlendCanvasGroupAlpha(cg, 0f, 2f, true);
        }
    }
}
