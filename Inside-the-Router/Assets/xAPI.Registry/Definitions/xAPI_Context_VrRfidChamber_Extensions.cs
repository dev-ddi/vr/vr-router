
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context vrRfidChamber as public properties.
    /// </summary>
    public sealed class xAPI_Context_VrRfidChamber_Extensions {

        public xAPI_Context_VrRfidChamber_Extensions() {
        }

        /// <summary>
        /// 10 extensions of 'vrRfidChamber': 5 in context, 5 in result.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/vrRfidChamber/extensions/context
        /// </summary>
        public xAPI_Extensions_Context_VrRfidChamber context {
            get {
                return new xAPI_Extensions_Context_VrRfidChamber();
            }
        }

        /// <summary>
        /// 10 extensions of 'vrRfidChamber': 5 in context, 5 in result.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/vrRfidChamber/extensions/result
        /// </summary>
        public xAPI_Extensions_Result_VrRfidChamber result {
            get {
                return new xAPI_Extensions_Result_VrRfidChamber();
            }
        }
    }
}