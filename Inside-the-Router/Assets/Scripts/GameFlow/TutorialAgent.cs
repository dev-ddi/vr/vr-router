using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//code for audio source loudness taken and modified from here: https://answers.unity.com/questions/1167177/how-do-i-get-the-current-volume-level-amplitude-of.html
public class TutorialAgent : MonoBehaviour
{
    public Vector3 DesiredLocation;
    public float MoveSpeed = 10f;
    public AudioSource Source;
    public Animator Animator;

    public float updateStep = 0.03f;
    public int sampleDataLength = 1024;

    private float currentUpdateTime = 0f;

    private float clipLoudness;
    private float[] clipSampleData;

    private void Awake()
    {
        DesiredLocation = transform.position;
        clipSampleData = new float[sampleDataLength];
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, DesiredLocation, Time.deltaTime * MoveSpeed);
        
        currentUpdateTime += Time.deltaTime;
        if (Source.clip != null)
        {
            if (currentUpdateTime >= updateStep)
            {
                currentUpdateTime = 0f;
                try
                {
                    Source.clip.GetData(clipSampleData, Source.timeSamples); //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
                }
                catch (System.Exception)
                {
                    clipSampleData = new float[sampleDataLength];
                    throw;
                }
                clipLoudness = 0f;
                foreach (var sample in clipSampleData)
                {
                    clipLoudness += Mathf.Abs(sample);
                }
                clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
            }
        }
        else {
            clipLoudness = 0f;
        }
        Animator.SetBool("Talk", clipLoudness > 0.02f);
    }

}
