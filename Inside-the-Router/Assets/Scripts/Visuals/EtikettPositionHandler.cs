using UnityEngine;

public class EtikettPositionHandler : MonoBehaviour
{
    TextMesh tm;
    void Start()
    {
        transform.position = transform.parent.position;
        tm = GetComponent<TextMesh>();
        tm.anchor = TextAnchor.MiddleCenter;
    }

    void Update()
    {
        var m_Camera = Camera.main; //TODO: maybe dont do this in update, as it is bad for performance, basically a searching the entire scene every frame
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
    }
}