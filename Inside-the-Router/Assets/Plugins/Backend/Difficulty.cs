﻿public enum Difficulty
{
    Random,     //Random Packages
    Internal,       //Internal Packages
    Outgoing,     //Outgoing Packages
    Incoming        //Incoming Packages
}
